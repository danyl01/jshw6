function createNewUser() {
    let name = prompt("Input first name");
    let surname= prompt("Input last name");
    let date = prompt("Input birthday");
    return {
        firstName: name, lastName: surname, birthday: date, getLogin() {
            return (this.firstName.charAt(0) + this.lastName).toLowerCase();
        } , getAge() {
            let today = new Date()
            let year = +this.birthday.substring(6);
            let month = +this.birthday.substring(4,5);
            let birthday = new Date(year,month);
            return today.getFullYear() - birthday.getFullYear();
        } , getPassword() {
            return this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(6)
        }
    };
}
let user = createNewUser();
console.log(user);
console.log(user.getAge());
console.log(user.getPassword());

